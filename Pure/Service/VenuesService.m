//
//  VenuesService.m
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import "VenuesService.h"
#import "APIClient.h"
#import <BloodMagic/Injectable.h>
#import "Bar.h"

NSString * const kClientId = @"0IMKMU0BMCZYHHNKTSFZN22IR4H3RZQ2AABMAW1NKW5TDRZF";
NSString * const kClientSecret = @"2CDV00MUCEFL1ZCMPINAQLIDWUVQ2KPO2ABTAYBQQHS4LXZC";
NSString * const kBarsCategoryId = @"4bf58dd8d48988d116941735";
NSString * const kVersionParameter = @"20140806";

@interface VenuesService () <BMInjectable>

@property (strong, bm_injectable) APIClient *client;

@end

@implementation VenuesService

@dynamic client;

- (void)barsForSearchTerm:(NSString*)searchTerm atLongitude:(double)lon andLattitude:(double)lat completion:(void (^)(NSArray *, NSError *))completion {
    NSString *path = @"venues/search";
    NSDictionary* params = @{
                             @"ll":[NSString stringWithFormat:@"%.1f,%.1f",lat,lon],
                             @"query":searchTerm ? searchTerm:@"beer",
                             @"client_id":kClientId,
                             @"client_secret":kClientSecret,
                             @"categoryId":kBarsCategoryId,
                             @"v":kVersionParameter
                                                    };
    [self.client GET:path parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = nil;
        NSDictionary* tmpDict = responseObject[@"response"];
        NSArray *barsResponse = [MTLJSONAdapter modelsOfClass:[Bar class] fromJSONArray:tmpDict[@"venues"] error:&error];
        if (error) {
            completion (nil, error);
            return;
        }
        completion (barsResponse, nil);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion (nil, error);
    }];
}

@end
