//
//  VenuesService.h
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VenuesService : NSObject

- (void)barsForSearchTerm:(NSString*)searchTerm atLongitude:(double)lon andLattitude:(double)lat completion:(void (^)(NSArray *, NSError *))completion;

@end
