//
//  VenueCategory.h
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface VenueCategory : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSString *iconPrefix;
@property (strong, nonatomic) NSString *iconSuffix;

@end
