//
//  VenueCategory.m
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import "VenueCategory.h"

@implementation VenueCategory

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"iconPrefix":@"icon.prefix",
             @"iconSuffix":@"icon.suffix"
             };
   
}

@end
