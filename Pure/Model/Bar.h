//
//  Bar.h
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import <Mantle/Mantle.h>

@class VenueLocation;

@interface Bar : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) VenueLocation *location;
@property (strong, nonatomic) NSArray *categories;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSNumber *totalCheckins;
@property (strong, nonatomic) NSNumber *currentCheckins;
@property (strong, nonatomic) NSString *website;

@end
