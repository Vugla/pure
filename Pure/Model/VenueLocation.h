//
//  VenueLocation.h
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface VenueLocation : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSString *address;
//@property (strong, nonatomic) NSString *crossStreet;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *postalCode;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSNumber *lat;
@property (strong, nonatomic) NSNumber *lng;
@property (strong, nonatomic) NSNumber *distance;

@end
