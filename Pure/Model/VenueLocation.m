//
//  VenueLocation.m
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import "VenueLocation.h"

@implementation VenueLocation

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"address":@"address",
//             @"crossStreet":@"crossStreet",
             @"city":@"city",
             @"state":@"state",
             @"postalCode":@"postalCode",
             @"country":@"country",
             @"lat":@"lat",
             @"lng":@"lng",
             @"distance":@"distance"
             };
}

@end
