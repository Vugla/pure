//
//  Bar.m
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import "Bar.h"
#import "VenueLocation.h"
#import "VenueCategory.h"

@implementation Bar

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"identifier":@"id",
             @"name":@"name",
             @"location":@"location",
             @"categories":@"categories",
             @"phone":@"contact.formattedPhone",
             @"totalCheckins":@"stats.checkinsCount",
             @"currentCheckins":@"hereNow.count",
             @"website":@"url"
             };
}

+ (NSValueTransformer *)locationJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:VenueLocation.class];
}

+ (NSValueTransformer *) categoriesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[VenueCategory class]];
}

@end
