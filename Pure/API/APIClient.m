//
//  APIClient.m
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import "APIClient.h"
#import <BloodMagic/Initializers.h>
#import <BloodMagic/Injectable.h>

#define BASE_URL @"https://api.foursquare.com/v2/"

@implementation APIClient

- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    
    return self;
}

//this method is executed before main.m
__attribute__((constructor)) static void APIClientInitialazer() {
    BMInitializer *initializer = [BMInitializer injectableInitializer];
    initializer.propertyClass = [APIClient class];
    initializer.initializer = ^id (id sender) {
        static id singleInstance = nil;
        static dispatch_once_t once;
        dispatch_once(&once, ^{
            singleInstance = [[APIClient alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        });
        return singleInstance;
    };
    [initializer registerInitializer];
}

@end
