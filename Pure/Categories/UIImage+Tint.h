//
//  UIImage+Tint.h
//  Pure
//
//  Created by Predrag Samardzic on 18/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Tint)

- (UIImage *)imageTintedWithColor:(UIColor *)color;

@end
