//
//  main.m
//  Pure
//
//  Created by Predrag Samardzic on 16/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
