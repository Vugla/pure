//
//  BarsViewController.h
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BarsViewController : UIViewController

@property(strong,nonatomic) NSString* searchTerm;

@end
