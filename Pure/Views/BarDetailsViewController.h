//
//  BarDetailsViewController.h
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bar.h"

@interface BarDetailsViewController : UIViewController

@property(strong,nonatomic) Bar* bar;

@end
