//
//  BarsViewController.m
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import "BarsViewController.h"
#import "VenuesService.h"
#import <BloodMagic/Lazy.h>
#import <CoreLocation/CoreLocation.h>
#import "Bar.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "VenueCategory.h"
#import "VenueLocation.h"
#import <GoogleMaps/GoogleMaps.h>
#import "BarDetailsViewController.h"
#import "UIImage+Tint.h"

@interface BarsViewController () <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, BMLazy, GMSMapViewDelegate>

@property (strong, nonatomic, bm_lazy) VenuesService *venuesService;
@property (strong,nonatomic) NSArray* bars;
@property (strong,nonatomic) CLLocationManager* locationManager;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (strong,nonatomic) NSMutableArray* pins;
@property (strong,nonatomic) NSMutableDictionary* pinIcons;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation BarsViewController

bool fromMarker;
@dynamic venuesService;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view bringSubviewToFront:self.activityIndicator];
    
    self.pins = [[NSMutableArray alloc]init];
    self.pinIcons = [[NSMutableDictionary alloc]init];
    
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager requestWhenInUseAuthorization];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    [self.locationManager requestLocation];
    
    self.mapView.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    fromMarker = YES;
    [self performSegueWithIdentifier:@"toDetails" sender:marker.userData];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    if (locations.count > 0) {
        CLLocation* location = [locations firstObject];
        [self loadData:location];
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                                longitude:location.coordinate.longitude
                                                                     zoom:12];
        [self.mapView setCamera:camera];
        self.mapView.myLocationEnabled = YES;
    }else{
        [self showMessage:@"Couldn't get your location" withTitle:@"No location"];
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    [self showMessage:@"Couldn't get your location" withTitle:@"No location"];
    
}

#pragma mark - UITableViewDelegate&DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.bars.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [self cellForIndexPath:indexPath];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    fromMarker = NO;
    [self performSegueWithIdentifier:@"toDetails" sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(UITableViewCell*)cellForIndexPath:(NSIndexPath*)indexPath{
    
    Bar* bar = self.bars[indexPath.row];
    VenueCategory* category = [bar.categories firstObject];
    UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"BarCell" forIndexPath:indexPath];
    cell.imageView.tintColor = [UIColor blackColor];
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@44%@",category.iconPrefix,category.iconSuffix]] placeholderImage:[[UIImage imageNamed:@"barPlaceholder"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        cell.imageView.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }];
    cell.textLabel.text = bar.name;
    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@,%@ %@,%@",bar.location.address?bar.location.address:@"",bar.location.postalCode?bar.location.postalCode:@"",bar.location.city?bar.location.city:@"",bar.location.country?bar.location.country:@""];
    return cell;
    
}


- (void)loadData:(CLLocation*)location {
    
    __weak __typeof__(self) welf = self;
    [self.activityIndicator startAnimating];
    [self.venuesService barsForSearchTerm:self.searchTerm atLongitude:location.coordinate.longitude andLattitude:location.coordinate.latitude completion:^(NSArray *bars, NSError *error) {
    
        typeof(self) sself = welf;
        [sself.activityIndicator stopAnimating];
        if (error) {
            [self showMessage:@"Error fetching data from server" withTitle:@"Server error"];
            return;
        }else if (bars.count == 0) {
            [self showMessage:[NSString stringWithFormat: @"No bars found aroun you for search term \"%@\"",self.searchTerm] withTitle:@"No results"];
        }
        sself.bars = bars;
         [sself reloadMarkers];
        [sself.tableView reloadData];
       
    }];

}

- (void)reloadMarkers {
    for (int i=0; i<self.bars.count; i++) {
        Bar* bar = self.bars[i];
        VenueCategory* category = [bar.categories firstObject];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake([bar.location.lat doubleValue], [bar.location.lng doubleValue]);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = bar.name;
        marker.icon = [UIImage imageNamed:@"pinPlaceholder"];
        marker.userData = bar;
        [self.pins addObject:marker];
        
        UIImageView* dummyView = [[UIImageView alloc]initWithFrame:CGRectZero];
    
        [self.pinIcons setObject:dummyView forKey:[NSString stringWithFormat:@"%ld",(long)i]];
        __weak __typeof__(self) welf = self;
        [dummyView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@32%@",category.iconPrefix,category.iconSuffix]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (!error) {
                typeof(self) sself = welf;
                [sself reloadMarkerAtIndex:i];
            }
            
        }];

       
    }
}

- (void)reloadMarkerAtIndex:(NSInteger)index {
    
    GMSMarker *marker = self.pins[index];
    marker.icon = [(UIImage*)((UIImageView*)[self.pinIcons objectForKey:[NSString stringWithFormat:@"%ld",(long)index]]).image imageTintedWithColor:[UIColor blackColor]];
    marker.map = self.mapView;
    
}

// Show an alert message
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"toDetails"]){

        BarDetailsViewController* controller = segue.destinationViewController;
        if (fromMarker) {
            controller.bar = sender;
        }else{
            NSIndexPath* selectedIndexPath = [self.tableView indexPathForSelectedRow];
            controller.bar = self.bars[selectedIndexPath.row];
        }
        
    }
}

@end
