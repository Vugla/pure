//
//  BarDetailsViewController.m
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import "BarDetailsViewController.h"
#import "VenueLocation.h"

@interface BarDetailsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *phoneLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalCheckinsLbl;
@property (weak, nonatomic) IBOutlet UILabel *currentCheckinsLbl;
@property (weak, nonatomic) IBOutlet UIButton *websiteBtn;

@end

@implementation BarDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLabels];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupLabels {
    self.nameLbl.text = self.bar.name;
    self.addressLbl.text = [NSString stringWithFormat:@"%@,%@ %@,%@",self.bar.location.address?self.bar.location.address:@"",self.bar.location.postalCode?self.bar.location.postalCode:@"",self.bar.location.city?self.bar.location.city:@"",self.bar.location.country?self.bar.location.country:@""];;
    self.phoneLbl.text = self.bar.phone ? self.bar.phone:@" - " ;
    self.totalCheckinsLbl.text = self.bar.totalCheckins ? [NSString stringWithFormat:@"%d",[self.bar.totalCheckins intValue]]:@"0" ;
    self.currentCheckinsLbl.text = self.bar.currentCheckins ? [NSString stringWithFormat:@"%d",[self.bar.currentCheckins intValue]]:@"0" ;
    if (self.bar.website) {
        [self.websiteBtn setTitle:self.bar.website forState:UIControlStateNormal];
        [self.websiteBtn addTarget:self action:@selector(goToWebsite:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (IBAction)goToWebsite:(id)sender {
    NSURL* url = [NSURL URLWithString:self.bar.website];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
