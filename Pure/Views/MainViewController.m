//
//  MainViewController.m
//  Pure
//
//  Created by Predrag Samardzic on 17/11/15.
//  Copyright © 2015 pedja. All rights reserved.
//

#import "MainViewController.h"
#import "BarsViewController.h"

@interface MainViewController () <UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - SearchBar delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self performSegueWithIdentifier:@"toBars" sender:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"toBars"]){
        BarsViewController* controller = segue.destinationViewController;
        controller.searchTerm = self.searchBar.text;
        
    }
}
@end
